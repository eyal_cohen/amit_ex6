CREATE FUNCTION make_balance_zero_func() RETURNS trigger AS $$
BEGIN
IF old.Balance < 0 then
	raise exception 'Balance is negative, the account remain open';
ELSIF old.Balance = 0 then
	INSERT into Actions (AccountNum, ActionName, ActionDate, Amount) 
	values (old.AccountNum, 'close', CURRENT_DATE, (old.Balance));
ELSE
	INSERT into Actions (AccountNum, ActionName, ActionDate, Amount) 
	values (old.AccountNum, 'close', CURRENT_DATE, -(old.Balance));
END IF;
return old;
END;
$$ language plpgsql;

CREATE TRIGGER make_balance_zero
BEFORE DELETE on AccountBalance
for each row
execute procedure make_balance_zero_func();