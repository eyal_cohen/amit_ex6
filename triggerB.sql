CREATE FUNCTION check_balance_consistent_with_overdraft_func() 
RETURNS trigger AS $$
DECLARE
_Overdraft real;
BEGIN
select C.Overdraft into _Overdraft 
from Customers C
where C.AccountNum = old.AccountNum;

IF (new.Balance < _Overdraft) then
	raise exception 'You can not withdraw this amount because it exceeds 
	the overdraft';
ELSE
	return new;
END IF;
END;
$$ language plpgsql;

CREATE TRIGGER check_balance_consistent_with_overdraft
BEFORE UPDATE on AccountBalance
for each row
execute procedure check_balance_consistent_with_overdraft_func();