DROP TABLE Customers, Actions, AccountBalance, Savings, Top10Customers;
DROP FUNCTION newCustomer(integer, varchar, varchar, real);
DROP FUNCTION closeCustomer(integer);
DROP FUNCTION doAction(integer, varchar, date, real);
DROP FUNCTION newsaving(integer, real, date, integer,real);
--create all tables
--create function newcustomer

select newCustomer(_CustomerID := 12, _CustomerName:='amit1', _CustomerPassword:= 'res1', _Overdraft:= -100);
select * from Customers;
select * from AccountBalance;
select newCustomer(_CustomerID := 12, _CustomerName:='amit1', _CustomerPassword:= 'res1', _Overdraft:= -100);
select newCustomer(_CustomerID := 123, _CustomerName:='amit2', _CustomerPassword:= 'res1', _Overdraft:= -100);
select * from Customers;
select * from AccountBalance;       
UPDATE Customers SET AccountStatus = 'close' WHERE CustomerID = 12;
DELETE from AccountBalance where AccountNum = 1;
select * from Customers;
select * from AccountBalance;       
select newCustomer(_CustomerID := 12, _CustomerName:='amit1', _CustomerPassword:= 'res1', _Overdraft:= -100);
select * from Customers;
select * from AccountBalance; 

--create function closecustomer

select closeCustomer(_CustomerID := 12);
select * from Customers;
select * from Actions;
select * from AccountBalance;
select * from Savings;
select * from Top10Customers;
select closeCustomer(_CustomerID := 12);

--create function doaction

select doAction(_CustomerID := 14, _ActionName:= 'saving', _ActionDate:= '07/02/92', _Amount:= 500);
select doAction(_CustomerID := 12, _ActionName:= 'stam', _ActionDate:= '07/02/92', _Amount:= 500);
select newCustomer(_CustomerID := 12, _CustomerName:='amit1', _CustomerPassword:= 'res1', _Overdraft:= -100);
select doAction(_CustomerID := 12, _ActionName:= 'receive', _ActionDate:= '07/02/92', _Amount:= 50000);
select * from Actions;
select * from AccountBalance;
select doAction(_CustomerID := 12, _ActionName:= 'saving', _ActionDate:= '07/02/92', _Amount:= -500);
select * from Actions;
select * from AccountBalance;
select doAction(_CustomerID := 12, _ActionName:= 'saving', _ActionDate:= '07/02/92', _Amount:= -500);
select doAction(_CustomerID := 12, _ActionName:= 'payment', _ActionDate:= '07/02/92', _Amount:= -100);
select * from Actions;
select * from AccountBalance;

--create function newSaving
select doAction(_CustomerID := 123, _ActionName:= 'receive', _ActionDate:= '07/02/92', _Amount:= 10000);
select newSaving(_CustomerID := 123, _Deposit:= 1000, _DepositDate:= '07/02/92', _NumOfYears:= 2, _Interest:=0.1);
select * from Actions;
select * from AccountBalance;
select * from Savings;