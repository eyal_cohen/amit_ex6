CREATE or replace function 
doAction(_CustomerID integer, _ActionName varchar, _ActionDate date, 
	_Amount real) returns integer as $$
DECLARE
v1 record;
_AccountNum integer;
_ActionNum integer;
BEGIN
select * into v1 from Customers C1 where C1.CustomerID = _CustomerID;
IF not found THEN 
	return -1;
ELSIF v1.AccountStatus = 'close' THEN
	return -1;
ELSIF _ActionName = 'receive' OR 
_ActionName = 'saving' OR _ActionName = 'payment' THEN
	select C.AccountNum into _AccountNum 
	from Customers C
	where C.CustomerID = _CustomerID;

	INSERT into Actions (AccountNum, ActionName, ActionDate, Amount) 
	values (_AccountNum, _ActionName, _ActionDate, _Amount)
	RETURNING ActionNum INTO _ActionNum;

	UPDATE AccountBalance
	SET Balance = Balance + _Amount
	WHERE AccountNum = _AccountNum;

	return _ActionNum;
ELSE
	return -1;
END IF;
END;
$$ language plpgsql