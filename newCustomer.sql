CREATE or replace function 
newCustomer(_CustomerID integer, _CustomerName varchar, 
	_CustomerPassword varchar, _Overdraft real) returns integer as $$
DECLARE
v1 record;
_AccountNum integer;
BEGIN
select * into v1 from Customers C1 where C1.CustomerID = _CustomerID;
IF not found THEN 
	insert into Customers (CustomerID, CustomerName,
	CustomerPassword, AccountStatus, Overdraft) values (_CustomerID, 
	_CustomerName, _CustomerPassword, 'open', _Overdraft)
	RETURNING AccountNum INTO _AccountNum;
	
	insert into AccountBalance (AccountNum, Balance) values (_AccountNum, 0);
	
	return _AccountNum;
ELSIF v1.AccountStatus = 'close' THEN
	UPDATE Customers 
	SET (CustomerName, CustomerPassword, AccountStatus, Overdraft) = 
	(_CustomerName, _CustomerPassword, 'open', _Overdraft)
	WHERE CustomerID = _CustomerID;
	
	select C2.AccountNum into _AccountNum 
	from Customers C2 
	where C2.CustomerID = _CustomerID;
	
	insert into AccountBalance (AccountNum, Balance) values (_AccountNum, 0);
	
	return _AccountNum;
ELSE
	return -1;
END IF;
END;
$$ language plpgsql