CREATE FUNCTION update_top_ten_func() 
RETURNS trigger AS $$
DECLARE
i record;
_counter integer := 1;
_TenPlaceBalance integer;
BEGIN
	DELETE FROM Top10Customers;
	
	FOR i in 
	select * 
	from Customers natural join AccountBalance
	where AccountStatus = 'open' and Balance > 0
	order by Balance DESC LOOP
		IF _counter < 10 THEN
			INSERT into Top10Customers (AccountNum, Balance) 
			values (i.AccountNum, i.Balance);
			_TenPlaceBalance = i.Balance;
			_counter = _counter + 1;
		ELSE
			IF i.Balance = _TenPlaceBalance THEN
				INSERT into Top10Customers (AccountNum, Balance) 
				values (i.AccountNum, i.Balance);
				_counter = _counter + 1;
			ELSE
				EXIT;
			END IF;
		END IF;
	END LOOP;

RETURN new;
END;
$$ language plpgsql;

CREATE TRIGGER update_top_ten
AFTER INSERT or DELETE or UPDATE on AccountBalance
for each row
execute procedure update_top_ten_func();