package CheckMySavings;

import java.sql.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class CheckMySavings {

	private Connection con;
	private Statement stmt;
	private ResultSet rs;

	private final int DepositIndex = 3;
	private final int DepositDateIndex = 4;
	private final int NumOfYearsIndex = 5;
	private final int InterestIndex = 6;

	public void init(String username) throws ClassNotFoundException 
	{
		try 
		{
			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection("jdbc:postgresql://dbcourse/public?user" + username) ;
			stmt = con.createStatement();
		} 
		catch (SQLException e) 
		{
			while (e != null) 
			{
				System.out.println(e.getSQLState());
				System.out.println(e.getMessage());
				System.out.println(e.getErrorCode());
				e = e.getNextException();
			}
			close();
			System.exit(-1);
		}
	}

	public void close() 
	{
		try 
		{
			stmt.close();
			rs.close();
			con.close();
		} catch (SQLException e) 
		{
			while (e != null) 
			{
				System.out.println(e.getSQLState());
				System.out.println(e.getMessage());
				System.out.println(e.getErrorCode());
				e = e.getNextException();
			}
			System.exit(-1);
		}
	}

	private int getDiffYears(Calendar a, Calendar b) 
	{
		int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
		if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH)
				|| (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && 
				a.get(Calendar.DAY_OF_MONTH) > b.get(Calendar.DAY_OF_MONTH))) 
		{
			diff--;
		}
		return diff;
	}

	private double checkMySavingsHelper(double deposit, int numOfYears, double interest) 
	{
		if (numOfYears == 1) 
		{
			return deposit * (1 + interest);
		} 
		else 
		{
			return (deposit + checkMySavingsHelper(deposit, numOfYears - 1, interest)) * (1 + interest);
		}
	}

	public double checkMySavings(int AccountNum, Date openDate) 
	{
		double result = 0;
		try 
		{
			rs = stmt.executeQuery("select * from Customers where AccountNum =" + AccountNum);
			if (!rs.isBeforeFirst())
			{
				System.out.println("There is no such AccountNum");
				close();
				System.exit(-1);
			}
			rs = stmt.executeQuery("select * from Savings where AccountNum =" + AccountNum);
			if (!rs.isBeforeFirst())
			{
				return 0;
			}
			while (rs.next())
			{
				java.sql.Date depositDate = rs.getDate(DepositDateIndex);
				int mumOfYears = rs.getInt(NumOfYearsIndex);
				// convert to calendar object
				Calendar depositDateCal = new GregorianCalendar();
				depositDateCal.setTime(depositDate);
				Calendar openDateCal = new GregorianCalendar();
				openDateCal.setTime(openDate);
				Calendar updateDepositDateCal = new GregorianCalendar();
				updateDepositDateCal.setTime(depositDate);
				updateDepositDateCal.add(Calendar.YEAR, mumOfYears);
				double deposit = rs.getDouble(DepositIndex);
				double interest = rs.getDouble(InterestIndex);

				if (openDateCal.before(depositDateCal))
				{
					return -1;
				}
				if (updateDepositDateCal.after(openDateCal)) 
				{
					int elapsedTime = getDiffYears(depositDateCal, openDateCal);
					result += deposit * (elapsedTime + 1);
				} 
				else 
				{
					result += checkMySavingsHelper(deposit, mumOfYears, interest);
				}
			}
		} 
		catch (SQLException e) 
		{
			while (e != null) 
			{
				System.out.println(e.getSQLState());
				System.out.println(e.getMessage());
				System.out.println(e.getErrorCode());
				e = e.getNextException();
			}
			close();
			System.exit(-1);
		}
		return result;
	}
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws ClassNotFoundException 
	{
		CheckMySavings a = new CheckMySavings(); 
		a.init("postgres");
		Date openDate = new Date(115, 11, 01);
		System.out.println(a.checkMySavings(5, openDate));
		
	}
}
