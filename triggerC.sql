CREATE FUNCTION check_date_A_func() 
RETURNS trigger AS $$
BEGIN
IF (new.ActionDate != CURRENT_DATE) then
	new.ActionDate = CURRENT_DATE;
	return new;
ELSE
	return new ;
END IF;
END;
$$ language plpgsql;

CREATE TRIGGER check_date_A
BEFORE INSERT on Actions
for each row
execute procedure check_date_A_func();



CREATE FUNCTION check_date_S_func() 
RETURNS trigger AS $$
BEGIN
IF (new.DepositDate != CURRENT_DATE) then
	new.DepositDate = CURRENT_DATE;
	return new;
ELSE
	return new;
END IF;
END;
$$ language plpgsql;

CREATE TRIGGER check_date_S
BEFORE INSERT on Savings
for each row
execute procedure check_date_S_func();