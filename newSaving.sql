CREATE or replace function 
newSaving(_CustomerID integer, _Deposit real, _DepositDate date, 
	_NumOfYears integer, _Interest real) returns integer as $$
DECLARE
v1 record;
_AccountNum integer;
_SavingNum integer;
BEGIN
select * into v1 from Customers C1 where C1.CustomerID = _CustomerID;
IF not found THEN 
	return -1;
ELSIF v1.AccountStatus = 'close' THEN
	return -1;
ELSE
	select C.AccountNum into _AccountNum 
	from Customers C
	where C.CustomerID = _CustomerID;

	INSERT into Savings (AccountNum, Deposit, DepositDate, NumOfYears, Interest) 
	values (_AccountNum, _Deposit, _DepositDate, _NumOfYears, _Interest)
	RETURNING SavingNum INTO _SavingNum;

	PERFORM doAction(_CustomerID, 'saving', _DepositDate, -(_Deposit));

	return _SavingNum;
END IF;
END;
$$ language plpgsql