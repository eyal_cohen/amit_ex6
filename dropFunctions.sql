DROP FUNCTION newCustomer(integer, varchar, varchar, real);
DROP FUNCTION closeCustomer(integer);
DROP FUNCTION doAction(integer, varchar, date, real);
DROP FUNCTION newsaving(integer, real, date, integer,real);

DROP TRIGGER make_balance_zero ON AccountBalance;
DROP FUNCTION make_balance_zero_func();

DROP TRIGGER check_balance_consistent_with_overdraft ON AccountBalance;
DROP FUNCTION check_balance_consistent_with_overdraft_func();

DROP TRIGGER check_date_A ON Actions;
DROP FUNCTION check_date_A_func();

DROP TRIGGER check_date_S ON Savings;
DROP FUNCTION check_date_S_func();

DROP TRIGGER update_top_ten ON AccountBalance;
DROP FUNCTION update_top_ten_func();
