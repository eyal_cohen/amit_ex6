select '** DROPPING TABLES AND FUNCTION **';
DROP TABLE Customers, Actions, AccountBalance, Savings, Top10Customers;
\ir dropFunctions.sql;


select '** CREATING TABLES **';
\ir createTables.sql;
-- \ir helperFunctions.sql;

--\ir test_01.sql;
--\ir test_02.sql;
--\ir test_03.sql;
\ir test_04.sql;