CREATE or replace function 
closeCustomer(_CustomerID integer) returns integer as $$
DECLARE
v1 record;
_AccountNum integer;
_Balance integer;
BEGIN
select * into v1 from Customers C1 where C1.CustomerID = _CustomerID;
IF not found THEN 
	return -1;
ELSIF v1.AccountStatus = 'close' THEN
	return -1;
ELSE
	select C2.AccountNum into _AccountNum 
	from Customers C2 
	where C2.CustomerID = _CustomerID;

	select A.Balance into _Balance 
	from AccountBalance A 
	where A.AccountNum = _AccountNum;
	
	--INSERT into Actions (AccountNum, ActionName, ActionDate, Amount) 
	--values (_AccountNum, 'close', CURRENT_DATE, -(_Balance));

	DELETE from AccountBalance where AccountNum = _AccountNum;
	DELETE from Savings where AccountNum = _AccountNum;
	DELETE from Top10Customers where AccountNum = _AccountNum;

	UPDATE Customers
	SET AccountStatus = 'close'
	WHERE CustomerID = _CustomerID;

	return _AccountNum;
END IF;
END;
$$ language plpgsql